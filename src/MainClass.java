
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainClass {  public static void main(String[] args) throws IOException {
    variableToValue("C:\\Users\\directory\\Desktop\\nawfal\\data", "C:\\Users\\directory\\Desktop\\nawfal\\data\\.env");
}

    static void variableToValue(String configs, String env) throws IOException {
         // try closable , to close files automatically after finishing
        // envLines contain the lines of the env file
       // ConfigFiles contains the lis of files in the config directory
        try (Stream<String> envLines = Files.lines(Paths.get(env)); Stream<Path> ConfigFiles = Files.list(Paths.get(configs))) {
            // Creating a map key value from the envlines : key is the name of the variable , value is the value of it, with spliting by the "="
            Map<String, String> variables = envLines.collect(Collectors.toMap(k -> k.split("=")[0], v -> v.split("=")[1]));
            // wa take just the .config files and make sure that the file isn't a system file, then we loop after each file
            ConfigFiles.filter(path -> path.toString().endsWith(".config") && Files.isRegularFile(path))
                    // for every file
                    .forEach(file -> {
                        // we read the lines of the file
                        try (Stream<String> configLines = Files.lines(file)) {
                            List<String> toWrite = configLines.map(configLine -> {
                                //we check if the line contain the variable's regex, if not we return the line without changing it
                                if (Pattern.compile(".*\\$#\\w*#.*").matcher(configLine).find()) {
                                    // we find the variable by its regex
                                    Pattern pattern = Pattern.compile("\\$#\\w*#");
                                    Matcher matcher = pattern.matcher(configLine);
                                    // a list thats gonna contains the variables to replace for each line, i supposed that one line can contain multiple variables
                                    List<String> toReplace = new ArrayList<>();
                                    while (matcher.find()) {
                                        // we add all the variables in a line
                                        toReplace.add(matcher.group(0));
                                    }
                                    // replaceBy contains the values of the variables to replace, we remove the "$#" and the "#" at the end using substring
                                    String[] replaceBy = toReplace.stream()
                                            .map(entry -> variables.get(entry.substring(2, entry.length() - 1)))
                                            .toArray(String[]::new);
                                    //we change all the variables by their values in each line
                                    for (int i = 0; i < toReplace.size(); i++) {
                                        configLine = configLine.replace(toReplace.get(i), replaceBy[i]);
                                    }

                                }
                                //we return the changed line
                                return configLine;
                                //Transforming the string stream into a list
                            }).collect(Collectors.toList());
                            //Writing the new lines in the file
                            Files.write(file, toWrite);

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
        }
    }
}
